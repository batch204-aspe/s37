const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Import userRoutes from routes folder in order to access or use it to the server
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const port = process.env.PORT || 4000;

const app = express();

/********** MongoDB Connection Start **********/
mongoose.connect("mongodb+srv://admin:admin123@batch204-aspemarkjoseph.iwwi5jg.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// TO check connection of Database
let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDb Atlas!"));
/********** MongoDB Connection End **********/


app.use(cors());
app.use(express.json());



// localhost:4000/users/checkEmail
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);







app.listen(port, () => {
	console.log(`API is now online on port: ${port}`);
});