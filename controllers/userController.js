// TO call the models we need to import the User.js from models folder
const User = require("../models/User");
const Course = require("../models/Course");
// Import the bcrypt package in order to use the package
const bcrypt = require("bcrypt");

// Import the authjs using the require("") directive
const auth = require("../auth");


// Get ALL User
module.exports.getALLUser = () => {

	return User.find({}).then(result => {
		return result
	})
}

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {

			//.find is a mongoose method to find a specific value
	return User.find({ email : reqBody.email }).then(result => {

		if(result.length > 0) {

			return true

		} else {

			return false
		}
	})
}


// Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		/* ☼ bcrypt ► is a package and assigning it to a variable bcrypt in order to use the package
		   
		   ☼ .hashSync() method ► is a bcrypt method that is responsible for hashing a data
		   
		   ☼ Syntax:

		   		bcrypt.hashSync(<dataTObeHash>, <saltRound>)

		   			- saltround is the number of times to generate a password encryption 
		*/ 
		password: bcrypt.hashSync(reqBody.password, 10)

	});

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return true
		}

	})

}


// Controller for User Login / User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {

			return false

		} else {
						
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
						// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// compare.Sync is a bcrypt method in order to compare a data
			// bcrypt.compareSync(<dataToBeCompare>, <dataFromDB>)
			//  return Boolean

			if(isPasswordCorrect) {

				// Generate an access token
								// Uses the "createAccessToken" method defined in the "auth.js" file
								// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
								

				return { access: auth.createAccessToken(result)}

			} else { // Passwords do not match

				return false

			}

		}

	})

}


/********** Activity for S38 Start -- "getProfile" Controller  **********/
	/*
		1. Create a "/details" route that will accept the user's ID to retrieve the details of a user

		2. Create a GetProfile controller Method for retrieving the details of the user:

			a. Find the document in the database using user's ID

			b. Reassign the password of the returned document to an empty String

			c. Return the result back to the frontend

		3. Process a POST request at the /details route using postman to retrieve the details of the user.

	*/

// GetProfile Controller
module.exports.getProfile = (reqBody) => {


	return User.findById(reqBody.id).then(result => {

		result.password  = "";

		return result

	})

}


// Other Solution
/*
	
	module.exports.getProfile = (data) => {


	return User.findById(data.id).then(result => {

		result.password  = "";

		return result
	});
}

*/

/********** Activity for S38 End -- "getProfile" Controller  **********/





/*
	Authenticated User Enrollment in ACTIVE COURSE workflow
		1. user logs in, server will respond with JWT on successful authentication

		2. A POST request with the JWT in tis header and the course ID in tis request body will be sent to eh /users/enroll endpoint

		3. Servver validates JWT
			a. If valid user will be enrolled in the course
			b. If invalid deny request

*/

/*
	Mini-Activity
		- Refactor the uerRoute to make sure that only user can enroll.

*/

// Enroll User to a Class
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		// Add the course ID in the enrollments array of the user
		// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
		let isUserUpdated = await User.findById(data.userId).then( user => {

			// Adds the courseId in the user's enrollments array
			user.enrollments.push({courseId: data.courseId});

			// Saves the updated user information in the database
			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true
				
				}
		
			});
		});


		// Add the user ID in the enrollees array of the course
		// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

				// Adds the userId in the course's enrollees array
				course.enrollees.push({userId: data.userId})

				// Saves the updated course information in the database
				return course.save().then((course, error) => {

					if(error) {
						return false

					} else {
						
						return true
					}
				});
		});


		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated) {

			// User enrollment successful
			return true

		} else {

			// User enrollment failure
			return false
		}
	}
}