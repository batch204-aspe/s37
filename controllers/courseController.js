const Course = require("../models/Course");
const User = require("../models/User");


// Create a New Course
module.exports.addCourse = (reqBody) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	// Saves the created object to our database
	return newCourse.save().then( (course, error) => {

			// Course creation successful
			if(error) {

				return false

			} else { // Course creation failed

				return true

			}
	});
}

	
/*	Create a New Course - Solution 2

		module.exports.addCourse = (reqBody, userData) => {

		     return User.findById(userData.userId).then(result => {

		         if (userData.isAdmin == false) {
		             return false
		         } else {
		             let newCourse = new Course({
		                 name: reqBody.name,
		                 description: reqBody.description,
		                 price: reqBody.price
		             })
		        
		             //Saves the created object to the database
		             return newCourse.save().then((course, error) => {
		                 //Course creation failed
		                 if(error) {
		                     return false
		                 } else {
		                     //course creation successful
		                     return "Course creation successful"
		                }
		            })
		        }
		        
		    });    
		}
*/


// Retrieving All Courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});

}

// Retrieving All "Active" Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	});

}

// Retrieving a SPECIFIC Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})

}

// Updating a Specific Course
module.exports.updateCourse = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // Specify the fields/properties of the document to be updated
            let updatedCourse = {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            };

            // Syntax:
                        // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

                // Course not updated
                if(error) {

                    return false

                // Course updated successfully    
                } else {

                    return true
                }
            })

        } else {

            return false
        }

    })

}



/********** S40 ACTIVITY - ARCHIVING A COURSE Start **********/

/* Activity Code for S40 - Updating a Course Archive

		1. Create a route for "archiving" a course. THe route must use JWT authentication and obtain the course ID from the URL.

		2. create a controller method for archiving a course obtaining the course ID from request params and the course information from the request body.

		3. Process a PUT request at the "/:courseId/archive" route using postman to archive a course
*/
// Updating a Course Archive || Archive a Course
module.exports.updateCourseArchive = (data, reqBody) => {

	return Course.findById(data.courseId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return Course.findByIdAndUpdate(result._id, updateActiveField).then((course, err) => {

					if(err) {
					
						return false
					
					}  else {

						return true
					}
			})
			 
		} else {

			return false
		} 
	})
}

/* ========== Other Solution 1 START ==========
	
	module.exports.updateCourseArchive = (data) => {

	 	return Course.findById(data.courseId).then((result, err) => {

	 		if(data.payload === true) {

	 			result.isActive = false;

	 			return result.save().then((archivedCourse, err) => {

	 				// Course not archived
	 				if(err) {

	 					return false;

	 				// Course archived successfully
	 				} else {

	 					return true;
	 				}
	 			})

	 		} else {

	 			//If user is not Admin
	 			return false
	 		}

	 	})
	 }
========== Other Solution 1 END ============ */	

/********** S40 ACTIVITY - ARCHIVING A COURSE END **********/