const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

// Import the auth.js using the require("") directive
const auth = require("../auth");



// Route for Retrieving User
router.get("/showAllUsers", (req, res) => {

	userController.getALLUser().then(resultFromController => res.send(resultFromController));

});

// Route for checking if the user's email already exist in our database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for USer Registration / USer Authentication
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for User Login
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))

});	



/********** Activity for S38 Start -- "/details" Route  **********/
	/*
		1. Create a /details route that will accept the user's ID to retrieve the details of a user

		2. Create a GetProfile controller Method for retrieving the details of the user:

			a. Find the document in the database using user's ID

			b. Reassign the password of the returned document to an empty String

			c. Return the result back to the frontend

		3. Process a POST request at the /details route using postman to retrieve the details of the user.

	*/

// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can retrieve details
router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	/* UserData Output:
			  id: '634015c1ce2300965f92d324',
			  email: 'janehufano@mail.com',
			  isAdmin: false,
			  iat: 1665404779

	*/

	console.log(req.headers.authorization);
	/* req.headers.authorization Output: 

			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxMzZlOGY1ZjNlYzlmY2I1ZTIyMiIsImVtYWlsIjoibGV4dXMxMjNAbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjY1NDA3NDQ4fQ.NmCyQKYtPVdiPv74hjRJ_HD0LYfTzbmS0USRNkXsd_g

	*/

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))

});

/*	Other Solutions

	router.post("/details", (req, res) => {

		userController.getProfile({id: req.body.id}).then(resultFromController => res.send(resultFromController));

	});
*/

/********** Activity for S38 END  **********/



// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {


	let data = {

		// User ID and IsAdmin will be retrieved from the request header
		userId: auth.decode(req.headers.authorization).id,
		
		// Course ID will be retrieved from the request body
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));


});


module.exports = router;