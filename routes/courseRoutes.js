const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth.js');


/*
	Mini-Activity:
		limit the course creation to admin only. Refactor the course route/controller.
*/

//Route for creating a course - Solution 1
// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin


	if(isAdmin) {

		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	
	} else {

		return res.send("User is not an Admin");

	}
});

/* Route for creating a course - Solution 2
	
	router.post("/", auth.verify, (req, res) => {

	     const userData = auth.decode(req.headers.authorization)

	     courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
	 })



*/

// Route for Retrieving all the courses
router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

});

// Route for Retrieving all the "ACTIVE" courses
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));

});

// Retrieving specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId);
	console.log(req.params);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
			// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
			// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for Updating a Course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});



/********** S40 ACTIVITY - ARCHIVING A COURSE START **********/

/* Activity Code for S40 - Updating a Course Archive

		1. Create a route for "archiving" a course. THe route must use JWT authentication and obtain the course ID from the URL.

		2. create a controller method for archiving a course obtaining the course ID from request params and the course information from the request body.

		3. Process a PUT request at the "/:courseId/archive" route using postman to archive a course
*/

// Route for  course ARCHIVING
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
			courseId : req.params.courseId,
			payload : auth.decode(req.headers.authorization).isAdmin
		}

		courseController.updateCourseArchive(data, req.body).then(resultFromController => res.send(resultFromController))

	/* ========== Other Solution 1 START ==========
			
			const data = {
			 		courseId : req.params.courseId,
			 		payload : auth.decode(req.headers.authorization).isAdmin
			 	}

			 	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
		
	   ========== Other Solution 1 END ============ */

});
/********** S40 ACTIVITY - ARCHIVING A COURSE END **********/


module.exports = router;