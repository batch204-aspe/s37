const mongoose = require("mongoose");


/********** Activity for S37 Start --- "User" Model  **********/

	/* Create the User model that will be used in creating the user records and storing information relevant to our application

		1. Create a User Model with the ff. properties:

			a. firstName - String
			b. lastName - String
			c. email - String
			d. password - String
			e. isAdmin - String
			f. mobileNo - String
			g. enrollments - Array of Objects

					i.   courseId - String
					ii.  enrolledOn - Date (dafault value - new Date Object)
					iii. status - String (dafault value - Enrolled)
	*/

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is Required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
	},

	email: {
		type: String,
		required: [true, "Email is Required"]
	},

	password: {
		type: String,
		required: [true, "Password is Required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required"]

	},

	enrollments: [
		{
			
			courseId: {
				type: String,
				required: [true, "course Id is required"]
			},

			enrolledOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Enrolled"
			}	
		}
	]
});
module.exports = mongoose.model("User", userSchema);

/********** Activity for S37 END --- "User" Model  **********/